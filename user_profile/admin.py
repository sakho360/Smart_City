# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import OrderedDict

from django import forms

from django.utils.translation import ugettext, ugettext_lazy as _

from django.utils.safestring import mark_safe
import re

# Register your models here.

class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': _(mark_safe("<span id = change_error>The two password fields didn't match.</span>")),
    }
    new_password1 = forms.CharField(label=_("New password"),
                                    widget=forms.PasswordInput)
    new_password2 = forms.CharField(label=_("New password confirmation"),
                                    widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(SetPasswordForm, self).__init__(*args, **kwargs)

    def clean_new_password1(self):
        errors = []
        error = False
        password1 = self.cleaned_data.get('new_password1')
        if len(password1) <8:
            errors.append(forms.ValidationError(mark_safe("<p>&#10071; Password must be over 8 characters!</p>")))
            error = True
        if password1.isnumeric():
            errors.append(forms.ValidationError(mark_safe("<p>&#10071; Password must not be all numbers!</p>")))
            error = True
        if re.match("^[A-Za-z0-9]+$", password1):
            errors.append(forms.ValidationError(mark_safe("<p>&#10071; Password must contain special characters! (e.g. !@#$%^&*())</p>")))
            error = True

        if error == False:
            return password1
        else:
                raise forms.ValidationError(errors)




    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password2

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password1'])
        if commit:
            self.user.save()
        return self.user

class PasswordChangeForm(SetPasswordForm):
    """
    A form that lets a user change their password by entering their old
    password.
    """
    error_messages = dict(SetPasswordForm.error_messages, **{
        'password_incorrect': _("Your old password was entered incorrectly. "
                                "Please enter it again."),
    })
    old_password = forms.CharField(label=_("Old password"),
                                   widget=forms.PasswordInput)

    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password


PasswordChangeForm.base_fields = OrderedDict(
    (k, PasswordChangeForm.base_fields[k])
    for k in ['old_password', 'new_password1', 'new_password2']
)
