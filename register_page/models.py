# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User

from django.db.models.signals import post_save
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
# Create your models here.

class UserProfileManager(BaseUserManager):
    def create_user(self, username, email, type, given_names, family_name, profile_picture, password=None):
        if not username:
            raise ValueError('Username must be set')

        user = self.model(username=username, email=email, type=type, given_names=given_names, family_name=family_name,
                          profile_picture=profile_picture)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, given_names, family_name, profile_picture, password):
        user=self.create_user(username, email=email, given_names=given_names, family_name=family_name,
                              profile_picture=profile_picture,password=password)
        user.is_admin=True
        user.save(using=self._db)
        return user




class UserProfile(AbstractBaseUser):
    username = models.CharField(max_length = 120, default = "", unique=True)


    email = models.EmailField(max_length = 120, default= "")
    BOOL_CHOICES = (('',''),('Student', 'Student'), ('Businessman', 'Businessman'), ('Tourist', 'Tourist'))

    given_names       = models.CharField(max_length = 120, default = "")
    family_name       = models.CharField(max_length = 120, default = "")
    type              = models.CharField(max_length = 120, choices=BOOL_CHOICES, null=True)
    profile_picture   = models.ImageField(upload_to='profile_picture', blank=True, null=True)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserProfileManager

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS=['type', 'email']
    

    def has_perms(self, perms, ob=None):
        return True

    def has_model_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        self.is_admin






