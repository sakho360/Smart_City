# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-01 09:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register_page', '0005_auto_20170831_0746'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='type',
            field=models.BooleanField(choices=[('', ''), ('S', 'Student'), ('B', 'Businessman'), ('T', 'Tourist')], default=True),
        ),
    ]
