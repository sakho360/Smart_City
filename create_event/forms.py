from django import forms

from search_page.models import Event

class EventForm(forms.ModelForm):
	class Meta:
		model = Event
		fields = [
			"title",
			"address",
			"description",
			"type",
			"picture",
			"event_time"
		]