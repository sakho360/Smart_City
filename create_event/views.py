from django.shortcuts import render, redirect
from .forms import EventForm
from search_page.models import Event

# Create your views here.
def create_event(request):
	if request.method == 'POST':
		form = EventForm(request.POST, request.FILES)

		if form.is_valid():
			instance = form.save(commit=False)
			instance.save()

			return redirect('/events')
	else:
		form = EventForm()
	context = {
		"form": form,

	}
	return render(request, "create_event.html", context)