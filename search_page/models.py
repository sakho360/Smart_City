from django.db import models

# Create your models here.

class Place(models.Model):

	TYPE_CHOICES = (
			("College", "College"),
			("Library", "Library"),
			("Industry", "Industry"),
			("Hotel", "Hotel"),
			("Park", "Park"),
			("Zoo", "Zoo"),
			("Museum", "Museum"),
			("Restaurant", "Restaurant"),
			("Malls", "Malls")
		)

	title = models.CharField(max_length = 100)
	address = models.CharField(max_length = 100)
	description = models.CharField(max_length = 250)
	type = models.CharField(max_length= 20, choices = TYPE_CHOICES, default= "None")
	latitude = models.DecimalField(max_digits=10, decimal_places = 4, default= 1.0)
	longitude = models.DecimalField(max_digits=10, decimal_places = 4, default= 1.0)
	date_entered = models.DateTimeField(auto_now=False, auto_now_add=True)
	last_updated = models.DateTimeField(auto_now=True, auto_now_add=False)
	picture = models.ImageField(upload_to='picture', blank=True, null=True)

	#add picture for sprint 2

	def __str__(self):
		return self.title


class Event(models.Model):

	TYPE_CHOICES = (
			("Concert", "Concert"),
			("Movie", "Movie")
		)

	title = models.CharField(max_length = 100, unique=True)
	address = models.CharField(max_length = 100)
	description = models.CharField(max_length = 250)
	type = models.CharField(max_length= 50, choices = TYPE_CHOICES, default= "None")
	event_time = models.DateTimeField(auto_now=False, auto_now_add=False)
	date_entered = models.DateTimeField(auto_now=False, auto_now_add=True)
	last_updated = models.DateTimeField(auto_now=True, auto_now_add=False)
	picture = models.ImageField(upload_to='picture', blank=True, null=True)


	#add picture for sprint 2

	def __str__(self):
		return self.title
