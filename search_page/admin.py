from django.contrib import admin

# Register your models here.
from .models import Place, Event

class PlaceAdmin(admin.ModelAdmin):
	list_display = ["title", "last_updated","date_entered"]
	list_filter = ["last_updated", "date_entered"]
	search_fields = ["title", "address", "description"]
	class Meta:
		model = Place

class EventAdmin(admin.ModelAdmin):
	list_display = ["title", "event_time", "last_updated","date_entered"]
	list_filter = ["event_time","last_updated", "date_entered"]
	search_fields = ["title", "address", "description", "event_time"]
	class Meta:
		model = Event

admin.site.register(Event, EventAdmin)
admin.site.register(Place, PlaceAdmin)