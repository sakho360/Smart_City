from __future__ import unicode_literals
from register_page.admin import UserCreationForm
from register_page.models import *
from django.test import TestCase
from django import forms
from django.core.files import File
from django.test import Client

# Create your tests here.
class PlaceEventPageTests(TestCase):

    def setup(self):
        c = Client()
        c.get('http://127.0.0.1:8000/search/item', {'title': 'Coldplay Concert', 'address': '59 Gardens Point Rd', 'type': 'Concert', 'description':'Coldplay live'})

    # Place/Event Page view loads
    def test_LoadPEPage(self):
        response = self.client.get('http://127.0.0.1:8000/search/item?')

        self.assertEqual(response.status_code, 200)


    #Event Items Show
    def  test_PEPageContainsTitle(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
              {'title': 'Coldplay Concert', 'address': '59 Gardens Point Rd', 'type': 'Concert',
               'description': 'Coldplay live'})

        self.assertContains(response, "Coldplay Concert")

    def  test_PEPageContainsAddress(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
              {'title': 'Coldplay Concert', 'address': '59 Gardens Point Rd', 'type': 'Concert',
               'description': 'Coldplay live'})

        self.assertContains(response, "59 Gardens Point Rd")

    def  test_PEPageContainsType(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
              {'title': 'Coldplay Concert', 'address': '59 Gardens Point Rd', 'type': 'Concert',
               'description': 'Coldplay live'})

        self.assertContains(response, "Concert")

    def  test_PEPageContainsDescription(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
              {'title': 'Coldplay Concert', 'address': '59 Gardens Point Rd', 'type': 'Concert',
               'description': 'Coldplay live'})

        self.assertContains(response, "Coldplay live")



    #Event Items Show
    def  test_PEPageContainsTitle(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
              {'title': 'Coldplay Concert', 'address': '59 Gardens Point Rd', 'type': 'Concert',
               'description': 'Coldplay live'})

        self.assertContains(response, "Coldplay Concert")

    def  test_PEPageContainsAddress(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
              {'title': 'Coldplay Concert', 'address': '59 Gardens Point Rd', 'type': 'Concert',
               'description': 'Coldplay live'})

        self.assertContains(response, "59 Gardens Point Rd")

    def  test_PEPageContainsType(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
              {'title': 'Coldplay Concert', 'address': '59 Gardens Point Rd', 'type': 'Concert',
               'description': 'Coldplay live'})

        self.assertContains(response, "Concert")

    def  test_PEPageContainsDescription(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
              {'title': 'Coldplay Concert', 'address': '59 Gardens Point Rd', 'type': 'Concert',
               'description': 'Coldplay live'})

        self.assertContains(response, "Coldplay live")



    #Place Items Show
    def  test_PEPageContainsTitle2(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
              {'title': 'Inspire Health Services', 'address': '365 Montague Road', 'type': 'Industry',
               'description': 'Physiotherapy clinic', 'latitude': '-27.4840', 'longitude':'153.0050', 'picture': 'picture % 2Finspire.jpg'})

        self.assertContains(response, "Inspire Health Services")

    def  test_PEPageContainsAddress2(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
                                   {'title': 'Inspire Health Services', 'address': '365 Montague Road',
                                    'type': 'Industry',
                                    'description': 'Physiotherapy clinic', 'latitude': '-27.4840',
                                    'longitude': '153.0050', 'picture': 'picture % 2Finspire.jpg'})

        self.assertContains(response, "365 Montague Road")



    def  test_PEPageContainsType2(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
                                   {'title': 'Inspire Health Services', 'address': '365 Montague Road',
                                    'type': 'Industry',
                                    'description': 'Physiotherapy clinic', 'latitude': '-27.4840',
                                    'longitude': '153.0050', 'picture': 'picture % 2Finspire.jpg'})

        self.assertContains(response, "Industry")

    def test_PEPageContainsDescription2(self):
            response = self.client.get('http://127.0.0.1:8000/search/item',
                                       {'title': 'Inspire Health Services', 'address': '365 Montague Road',
                                        'type': 'Industry',
                                        'description': 'Physiotherapy clinic', 'latitude': '-27.4840',
                                        'longitude': '153.0050', 'picture': 'picture % 2Finspire.jpg'})

            self.assertContains(response, "Physiotherapy clinic")

    def test_PEPageContainsLatitude(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
                                   {'title': 'Inspire Health Services', 'address': '365 Montague Road',
                                    'type': 'Industry',
                                    'description': 'Physiotherapy clinic', 'latitude': '-27.4840',
                                    'longitude': '153.0050', 'picture': 'picture % 2Finspire.jpg'})

        self.assertContains(response, "-27.4840")

    def test_PEPageContainsLongitude(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
                                   {'title': 'Inspire Health Services', 'address': '365 Montague Road',
                                    'type': 'Industry',
                                    'description': 'Physiotherapy clinic', 'latitude': '-27.4840',
                                    'longitude': '153.0050', 'picture': 'picture % 2Finspire.jpg'})

        self.assertContains(response, "153.0050")

    def test_PEPageContainsPicture(self):
        response = self.client.get('http://127.0.0.1:8000/search/item',
                                   {'title': 'Inspire Health Services', 'address': '365 Montague Road',
                                    'type': 'Industry',
                                    'description': 'Physiotherapy clinic', 'latitude': '-27.4840',
                                    'longitude': '153.0050', 'picture': 'picture % 2Finspire.jpg'})



        self.assertContains(response, "picture % 2Finspire.jpg")








