# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class ItemPlaceEventPageConfig(AppConfig):
    name = 'item_place_event_page'
