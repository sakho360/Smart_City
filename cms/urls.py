
from cms.apphook_pool import apphook_pool
from cms.appresolver import get_app_patterns
from cms.constants import SLUG_REGEXP
from cms.views import details
from django.conf import settings
from django.conf.urls import include, url
from user_profile.views import display_profile, edit_profile, change_password

if settings.APPEND_SLASH:
    regexp = r'^(?P<slug>%s)/$' % SLUG_REGEXP
else:
    regexp = r'^(?P<slug>%s)$' % SLUG_REGEXP

if apphook_pool.get_apphooks():
    # If there are some application urls, use special resolver,
    # so we will have standard reverse support.
    urlpatterns = get_app_patterns()
else:
    urlpatterns = []


urlpatterns.extend([
    url(r'^cms_wizard/', include('cms.wizards.urls')),
    url(regexp, details, name='pages-details-by-slug'),
    url(r'^$', details, {'slug': ''}, name='pages-root'),
    #   url(r'^profile/edit/$', edit_profile, name='edit_profile'),
    #   url(r'^profile/edit/change_password/$', change_password, name='change_password'),
    #   url(r'^places/$', places, name='places'),
    #   url(r'^events/$', events, name='events'),
    #   url(r'^search/place/$', place, name='place'),
    #   url(r'^search/event/$', event, name='event'),
    #   url(r'^search/item$', item, name='item'),
])
