# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from login_page.forms import UserLoginForm
from django.http import HttpResponse, HttpResponseRedirect

from django.contrib.auth import login, authenticate, logout

# Create your views here.

def login_user(request):
    if request.method == 'POST':
        form = UserLoginForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('/')

    else:
        form = UserLoginForm()

    args = {'form': form}
    html = render(request, 'login_page.html', args)
    return HttpResponse(html)


def logout_user(request):
    logout(request)
    return HttpResponseRedirect("../login")
